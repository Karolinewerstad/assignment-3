import { combineReducers } from 'redux'
import { startupReducer } from './startupReducer'
import { sessionReducer } from './sessionReducer'

const appReducer = combineReducers({
    startupReducer,
    sessionReducer
})

export default appReducer