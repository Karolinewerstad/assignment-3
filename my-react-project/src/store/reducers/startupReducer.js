import { ACTION_STARTUP_ATTEMPTING, ACTION_STARTUP_ERROR, ACTION_STARTUP_SUCCESS } from "../actions/startupActions"

const initialState = {
    startupAttempting: false,
    startupError: '',
}

export const startupReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_STARTUP_ATTEMPTING:
            return {
                ...state,
                startupAttempting: true,
                startupError: ''
            }
            case ACTION_STARTUP_SUCCESS:
                return {
                    ...initialState
                }
            case ACTION_STARTUP_ERROR:
                return {
                    ...state,
                    startupAttempting: false,
                    startupError: action.payload
                }
            default:
                return state
    }
}