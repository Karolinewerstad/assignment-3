export const ACTION_STARTUP_ATTEMPTING = '[startup] ATTEMPT'
export const ACTION_STARTUP_SUCCESS = '[startup] SUCCESS'
export const ACTION_STARTUP_ERROR = '[startup] ERROR'

export const startupAttemptAction = credentials => ({
    type: ACTION_STARTUP_ATTEMPTING,
    payload: credentials
})

export const startupSuccessAction = profile => ({
    type: ACTION_STARTUP_SUCCESS,
    payload: profile
})

export const startupErrorAction = error => ({
    type: ACTION_STARTUP_ERROR,
    payload: error
})
