import { StartupAPI } from "../../StartupAPI"
import { sessionSetAction } from "../actions/sessionActions"

import { ACTION_STARTUP_ATTEMPTING, ACTION_STARTUP_SUCCESS, startupErrorAction, startupSuccessAction } from "../actions/startupActions"

export const startupMiddleware = ({dispatch}) => next => action => {
    next(action)

    if (action.type === ACTION_STARTUP_ATTEMPTING) {
        StartupAPI.startup(action.payload)
        .then(profile => {
            dispatch(startupSuccessAction(profile))
        })
        .catch(error => {
            dispatch(startupErrorAction(error.message))
        })
    }

    if (action.type === ACTION_STARTUP_SUCCESS) {
        dispatch( sessionSetAction(action.payload))
    }
}