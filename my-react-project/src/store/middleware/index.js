import { applyMiddleware } from "redux";
import { sessionMiddleware } from "./sessionMiddleware";
import { startupMiddleware } from "./startupMiddleware";

export default applyMiddleware(
    startupMiddleware,
    sessionMiddleware
)