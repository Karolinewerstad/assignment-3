import { React, useState } from 'react';
import './TranslationPage.css'
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import AppContainer from './hoc/AppContainer';

const TranslationPage = () => {
    //const history = useHistory();
    const { loggedIn } = useSelector(state => state.sessionReducer)
    const [ input, setInput ] = useState('')
    const [ translatedString, setTranslatedString ] = useState([])

    function letterToSign(letter, index) {
        const src = `signs/${letter}.png`
        return (
            <div className="sign" key={ index }><img src={ src } alt=""/></div>
        )
    }

    const onInputChange = event => {
        setInput(event.target.value)
    }
    

    function translateString() {
        const string = input
        const currentState = []
        setTranslatedString([])
        let index = 0
            for (const letter of string.toLowerCase()) {
                console.log(letter);
                if (letter === " ") {
                    currentState.push(<li>   </li>);
                } else if ((/[a-z]/).test(letter)) {
                    currentState.push(letterToSign(letter, index))
                }
                index++
            }
            setTranslatedString(currentState)
        }

    return (
        <>
        {!loggedIn && <Redirect to="/" />}
        {loggedIn && 
        <AppContainer>
            <header>
                <h2 align="left" id="headerText">Lost in Translation</h2>
            </header>
            <main>
                <form>
                    <input type="text" placeholder="Enter text" onChange={ onInputChange }></input>
                    <button id="translateButton" type="button" onClick={ translateString }>{'->'}</button>
                </form>
                <div id="translationBox">
                    <div id="listOfSigns">{ translatedString }</div>
                </div>
            </main>
        </AppContainer>
        }
        </>  
    )
}

export default TranslationPage;