function Sign(letter) {
    const src = `LostInTranslation_Resources/${letter}.png`
    return (
        <img src={ src } alt=""/>
    )
}

export default Sign