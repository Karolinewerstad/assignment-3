export const StartupAPI = {
    startup(credentials) {
        return fetch(`http://localhost:3000/users/${credentials.user}`)
        .then(async (response) => {
            if (!response.ok) {
                if (response.status === 404) {
                    fetch(`http://localhost:3000/users`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({...credentials, translations: []})
                    })
                    return credentials
                } else {
                    const { error = 'An unknown error occurred' } = await response.json()
                    throw new Error(error)
                }
            } 
            return response.json()
        })
    }
}