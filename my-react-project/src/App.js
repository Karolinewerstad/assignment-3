import React from 'react';
import StartupPage from './StartupPage';
import TranslationPage from './TranslationPage'
import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom'

function App() {

  return (
      <BrowserRouter className="App">
          <Switch>
            <Route exact path="/" component={StartupPage} />
            <Route path="/translation" component={TranslationPage} />
          </Switch>
      </BrowserRouter>
  );
}

export default App;
