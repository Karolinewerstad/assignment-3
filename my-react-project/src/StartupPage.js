import { React, useState } from 'react';
import './StartupPage.css';
//import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { startupAttemptAction } from './store/actions/startupActions';
import { Redirect } from 'react-router-dom';

function StartupPage() {

    const dispatch = useDispatch()
    const { loggedIn } = useSelector(state => state.sessionReducer)

    //const history = useHistory();

    const [ credentials, setCredentials ] = useState({
        user: ''
    })

    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]:event.target.value
        })
    } 

  //  const clickedArrow = () => {
        //When the arrow is clicked, the name is stored and the application routes to the translation page.
  //      history.push("/translation");
    //}

    const onFormSubmit = event => {
        event.preventDefault() //stops the page from reloading
        dispatch(startupAttemptAction(credentials))
    }

    return (
        <>
            {loggedIn && <Redirect to="/translation" />}
            {!loggedIn && 
            <>
                <header>
                    <h3 align="left" id="headerText">Lost in Translation</h3>
                </header>
                <main>
                    <h1>Lost in Translation</h1>
                    <h2>Get started</h2>
                    <form onSubmit={ onFormSubmit }>
                        <input id="user"
                        type="text" 
                        placeholder="What's your name?"
                        onChange={ onInputChange }></input>
                        <button type="submit">{'->'}</button>
                    </form>
                </main>
            </>
            }
        </>
    )
}

//function clickedArrow() {
    //When the arrow is clicked, the name is stored and the application routes to the translation page.
//}

export default StartupPage