To start the web application first start the json database with this command:
json-server --id user user.json

then run the application with:
npm start

I did not have time to finish all the functionality or to style it, but creating a new user, logging in and redirecting to and from page translation page based on whether or not you are logged in, and the translation it self works. :)
